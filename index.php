<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Onestic</title>
	<link rel="stylesheet" href="dist/css/main.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
	<link href="https://fonts.googleapis.com/css?family=Slabo+27px|Source+Sans+Pro" rel="stylesheet"> 
	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
</head>
<body>
	<header class="header">
		<div class="container">
			<div class="header__logo">
				<h1><b>on</b>estic</h1>
			</div>
			<nav class="header__menu">
				<div class="header__menu__icon">
					<div>
						<i class="fas fa-bars"></i>
					</div>
				</div>
				<ul class="header__menu--normal">
					<li><a href="#">Link1</a></li>
					<li><a href="#">Link2</a></li>
					<li><a href="#">Link3</a></li>
					<li><a href="#">Link4</a></li>
				</ul>
				<ul class="header__menu--responsive">
					<li><a href="#">Link1</a></li>
					<li><a href="#">Link2</a></li>
					<li><a href="#">Link3</a></li>
					<li><a href="#">Link4</a></li>
				</ul>	
			</nav>
			<div class="header__search">
				<div>
					<i class="fas fa-search"></i>
				</div>
			</div>
		</div>
	</header>
	<div class="main">
		<div class="filters">
			<div class="container">
				<div class="filters--responsive">
					<button>Show Filters</button>
				</div>
				<div class="filters--normal">
					<div class="filters__colors">
						<div class="color" style="background-color: green" value="Green"></div>
						<div class="color" style="background-color: red" value="Red"></div>
						<div class="color" style="background-color: blue" value="Blue"></div>
						<div class="color" style="background-color: pink" value="Pink"></div>
						<div class="color" style="background-color: purple" value="Purple"></div>
					</div>
					<div class="filters__sizes">
						<div class="size" value="S">S</div>
						<div class="size" value="M">M</div>
						<div class="size" value="L">L</div>
						<div class="size" value="XL">XL</div>
					</div>
					<div class="filters__reset">
						<button>Reset Filters</button>
					</div>
					<div class="filters__icon">
						<i class="fas fa-sort-amount-up"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="grid">
			<div class="container">
				<div id="grid-row" class="row"></div>
			</div>
		</div>
	</div>
	<footer class="footer">
		<h4>Enrique Gomez 2018</h4>
		<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
		<script src="dist/js/main.min.js"></script>
	</footer>
</body>
</html>