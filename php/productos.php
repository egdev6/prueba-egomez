<?php
  //Cabeceras indicando que se envian datos JSON.
  header('Content-Type: application/json');
  header('Cache-Control: no-cache, must-revalidate');
  header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
  // Configuración BD
  $servidor = "localhost";   
  $basedatos = "testing";
  $usuario = "root";
  $password = "cdiberico";
  
  //Crear la conexión al servidor y ejecutar la consulta.
  $conexion= new mysqli($servidor, $usuario, $password) or die(mysqli_error());
  mysqli_query($conexion,"SET NAMES 'utf8'");
  mysqli_select_db($conexion,$basedatos) or die(mysqli_error());

  //Párametro opción para determinar la consulta (select) a realizar 
  if (isset($_GET['opcion']))$opcion=$_GET['opcion'];
  else if (isset($_POST['opcion'])) $opcion=$_POST['opcion'];
  //Párametro para realizar la consulta de productos por color   
  if (isset($_GET['colors'])) $color=$_GET['colors'];
  else if (isset($_POST['colors'])) $color=$_POST['colors'];
  //Párametro para realizar la consulta de productos por talla   
  if (isset($_GET['sizes'])) $size=$_GET['sizes'];
  else if (isset($_POST['sizes'])) $size=$_POST['sizes'];

  //Concatenamos condicion de colores
  if(sizeOf($color)>0 || sizeOf($size)>0){
    $condition = "";
    $colorConcat= "";
    for($i=0;$i<sizeOf($color);$i++){
      $colorConcat .= "color like '".$color[$i]."'";
      if($i!=sizeOf($color)-1 && sizeOf($color)>1){
        $colorConcat .= " or ";
      }
    }
    //Concatenamos condicion de tallas
    $sizeConcat="";
    for($i=0;$i<sizeOf($size);$i++){
      $sizeConcat .= "size like '".$size[$i]."'";
      if($i!=sizeOf($size)-1 && sizeOf($size)>1){
        $sizeConcat .= " or ";
      }
    }

    if($colorConcat!="" && $sizeConcat!=""){
      $condition = "(".$colorConcat.") or (".$sizeConcat.")";
    }else if($sizeConcat==""){
      $condition = "(".$colorConcat.")";
    }else if($colorConcat==""){
      $condition = "(".$sizeConcat.")";
    }
  }

  // ------- selección de la operación --------   
  switch ($opcion) {
      case "all":
           $sql="select * from products order by position limit 12";     
           break;
      case "filters":
           $sql="select * from products where ".$condition." order by position";     
           break;    
  }
  $datos=null;
  $resultados=mysqli_query($conexion,$sql) or die(mysqli_error());
  if ($resultados != null){
    while ( $fila = mysqli_fetch_array($resultados))
    {
      // Almacenar en un array cada fila del recordset.
      $datos[]=$fila;                    
    }
    // función de PHP que convierte a formato JSON el array.
    echo json_encode($datos);              
  };
  mysqli_close($conexion);
?>

