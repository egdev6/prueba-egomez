$(document).ready(function(){
	var productos, isDesktop, colors=[], sizes=[];
	//Funcion de inicializacion
	function init(){
		setEvents();
		checkResize();
		loadAllProducts();
	}
	//Function de seteo de eventos
	function setEvents(){
		$(".header__menu__icon").on('click', showMenuResponsive);
		$(".filters--responsive button").on('click', showFilters);
		$(".color").on('click', loadProductsByFilter);
		$(".size").on('click', loadProductsByFilter);
		$(".filters__reset button").on('click', resetFilters);
		$(window).on('resize', checkResize);
	}
	//Funcion de redimension de redimensionado de pantalla
	function checkResize(e){
		if($(window).width() >= 980){
			isDesktop = true;
			$(".header__menu--responsive").css({display:'none'});
		}

		if($(window).width() >= 320){
			isDesktop = true;
			$(".filters--normal").css({display:'flex'});
		}
		else{
			isDesktop = false;
			$(".filters--normal").css({display:'none'});
		}
		checkResetFilters();
	}
	//Verificar si se tiene que mostrar boton de reset filters
	function checkResetFilters(){
		if(colors.length > 0 || sizes.length > 0){
			$(".filters__reset").css({'display':'block'});
		}else if(colors.length == 0 && sizes.length == 0){
			$(".filters__reset").css({'display':'none'});
		}
	}
	//Funcion que muestra/oculta menu responsive
	function showMenuResponsive(e){
		$(".header__menu--responsive").toggle({ direction: "left" }, 1000);
	}
	//Funcion que muestra/oculta filtros
	function showFilters(e){
		var filters = $(".filters--normal");
		filters.slideToggle();
	}
	//Peticion para cargar todos los productos
	function loadAllProducts(){
		var params = {
      "opcion" : 'all'
    };
    $.ajax({
      data: params,
      url: './php/productos.php',
      dataType: 'json',
      type: 'post', 
      success:  function (response) {
        showProducts(response);
      }
    });
	}
	//Peticion para cargar productos por colores y tallas (Filtros)
	function loadProductsByFilter(e){
		var filter = e.currentTarget.attributes.value.nodeValue;
		var params = {
      "opcion" : 'filters',
      "colors" : colors,
      "sizes" : sizes
    };

		if(e.currentTarget.className == 'color'){
			colors.push(filter);
		}else{
			sizes.push(filter);
		}
		$(e.currentTarget).addClass('active');
    $.ajax({
      data: params,
      url: './php/productos.php',
      dataType: 'json',
      type: 'post',
      success:  function (response) {
        showProducts(response);
      }
    }); 
    checkResetFilters();
	}
	//Mostramos los productos
	function showProducts(datosjson){
		var product="", name, description;
		if(datosjson){
			for(var i=0; i<datosjson.length;i++){
				name = datosjson[i].description.slice(0,15)+"...";
				if(isDesktop){
					description = datosjson[i].description.slice(0,55)+"...";
				}else{
					description = datosjson[i].description.slice(0,15)+"...";
				}
				product += "<div class='col-xs-6 col-md-4 col-lg-3'>"+
									"<div class='grid-item'>"+
										"<div class='grid-item__image'></div>"+
										"<div class='grid-item__info'>"+
											"<h4>"+ name +"</h4>"+
											"<p>"+ description +"</p>"+
											"<p>Color:"+ showColor(datosjson[i].color.toLowerCase()) +" Talla: <span>"+ datosjson[i].size +"</span></p>"+
											"<h3>"+ datosjson[i].price +"€</h3>"+
										"</div>"+
									"</div>"+
								"</div>";
			}
			document.getElementById('grid-row').innerHTML = product;
		}else{
			document.getElementById('grid-row').innerHTML = "<h2>No hay productos.</h2>";
		}
	}
	//Resetamos los filtros
	function resetFilters(e){
		$(".color").removeClass('active');
		$(".size").removeClass('active');
		colors = [];
		sizes = [];

		checkResetFilters();
		loadAllProducts();
	}
	//Mostramos color del producto en el grid
	function showColor(color){
		return "<button class='color' style='background-color:"+color+"' value='"+color+"'></button>";
	}
	//Inicializacion
	init();
});