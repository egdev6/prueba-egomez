// Include gulp
var gulp        = require('gulp');

// Include Our Plugins
var jshint      = require('gulp-jshint');
var sass        = require('gulp-sass');
var concat      = require('gulp-concat');
var uglify      = require('gulp-uglify');
var rename      = require('gulp-rename');
var imagemin    = require('gulp-imagemin');
var browserSync = require('browser-sync'); 

// Imagemin Task
gulp.task('imagemin', function () {
    return gulp.src('img/*')
        .pipe(imagemin({
            progressive: true
        }))
        .pipe(gulp.dest('dist/image'));
});
// Lint Task
gulp.task('lint', function() {
    return gulp.src('js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src('scss/main.scss')
        .pipe(sass())
        .pipe(gulp.dest('dist/css'));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src('js/*.js')
        .pipe(concat('main.js'))
        .pipe(gulp.dest('dist/js'))
        .pipe(rename('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));
});
//BrowserSync
gulp.task('browser-sync', function() {
    var files = [
        "./dist/css/*.css",
        "./dist/js/*.js",
        "./*.html"
    ];
    browserSync.init(files,{
        proxy : "localhost/prueba-onestic/"
    });
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('img/*', ['imagemin']);
    gulp.watch('js/*.js', ['lint', 'scripts']);
    gulp.watch('scss/**/*.scss', ['sass']);
    gulp.watch('watch').on( 'change', browserSync.reload);
});

// Default Task
gulp.task('default', ['imagemin', 'lint', 'sass', 'scripts', 'browser-sync', 'watch']);